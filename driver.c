#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>

#ifdef _WIN32
#define WINPAUSE system("pause")
#endif

#pragma warning(disable : 4996)

extern void sqrtX(double n, double* result);

int main(void)
{
	AllocConsole();
	freopen("CONIN$", "rb", stdin);
	freopen("CONOUT$", "wb", stdout);

	double result = 0;
	printf("Beginning sqrt\n.");
	sqrtX(1.0, &result);
	printf("End sqrt: ");
	printf(&result);
	_getche();
	return 0;
}