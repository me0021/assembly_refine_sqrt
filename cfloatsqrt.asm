.586
.MODEL FLAT
.STACK 4096
.DATA
.CODE

GLOBAL _sqrtX	PROC
	
	compare MACRO N
		fcom N
		fstsw ax
		sahf
	ENDM

	push ebp		; save base pointer
	mov ebp, esp	; load stack
	push ebx		; save ebx

	finit			; clear / init f regs

	fld REAL8 PTR [ebp+8]	; load first arg by value onto stack

	;DO CALCULATIONS HERE

	fld1
	fldadd

	;compare accuracy
	;jle		exitloop
	
	;END CALCULATIONS

exitloop:

	mov ebx,[ebp+16]	; load second arg by pointer 
	fstp REAL8 PTR [ebx] ;store value from fregs back in

	pop ebx
	pop ebp
	mov eax, 0
	ret

_sqrtX ENDP

	